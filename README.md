# Black Jack 3000

Black Jack 3000 is a python application that lets you embody a player and play Black Jack against a dealer.

# How to use

First, start by cloning the project on your computer:

```
git clone https://gitlab.com/svenbarray/projetconceptionlogiciel
cd path\to\project
```

Install the requirements:

```
pip install -r requirements.txt
```

Then you can run the app :

```
python main.py
```

Once you run it, the application and menus (visible on the terminal prompt) should be pretty self-explanatory. Just abide by the menus! You can create and delete players (the player database starts with a single player named Alice who has 2000 credits). You can then embody them and play Black Jack against a dealer, and try to gain as much credits as possible! 
You can find the rules here: https://bicyclecards.com/how-to-play/blackjack/

# Project Structure

The diagrams folder contains diagrams that can help understand the project structure better. It is recommended to check that one first.
The business_object contains the basic python objects used in this project (namely: decks, cards and hands).
The dao folder contains the scripts related to database management.
The service folder contains some functions that support the gameplay.
The view folder contains the whole menuing navigation the player sees when playing the game.
The players.db file at the root of the project is the player database file. 
Finally, the test folder contains unit tests that help ensure the program is working properly. You can launch the unit tests with the following command: 

```
python tests/service_gameplay_tests.py
```

Have fun!
