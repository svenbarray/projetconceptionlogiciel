from service.gameplay import get_card_worth

class Card():
    def __init__(self, value, suit):
        self.value = value
        self.suit = suit
        self.worth = get_card_worth(self.value)
