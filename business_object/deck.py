import requests

class Deck():
    def __init__(self):
        self.deck_id = requests.get("https://deckofcardsapi.com/api/deck/new/shuffle/?deck_count=6").json()["deck_id"]
