import requests
from business_object.card import Card

class Hand():
    def __init__(self):
        self.hand = []

    def draw(self, deck_id):
        draw = requests.get("https://deckofcardsapi.com/api/deck/" + str(deck_id) + "/draw/?count=1").json()["cards"][0]
        value = draw["value"].capitalize()
        suit = draw["suit"].capitalize()
        self.hand.append(Card(value, suit))

    def get_total_worth(self):
        total_worth = 0
        nb_ace = 0
        for card in self.hand:
            total_worth += card.worth
            if card.value == "Ace":
                nb_ace += 1
        while nb_ace > 0 and total_worth > 21:
            nb_ace -= 1
            total_worth -= 10
        return total_worth

    def __str__(self):
        hand_str = ""
        for card in self.hand:
            hand_str += card.value + " of " + card.suit + " / "
        hand_str = hand_str[:-3]
        return "Hand: " + hand_str + "\nTotal hand worth: " + str(self.get_total_worth())
