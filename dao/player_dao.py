import sqlite3

class Player_DAO():
    def __init__(self):
        pass

    def init_db(self):
        with sqlite3.connect('players.db') as cursor:
            cursor.execute("CREATE TABLE IF NOT EXISTS PLAYER (name TEXT UNIQUE PRIMARY KEY, balance INTEGER)")

    def insert_player(self, name, balance):
        with sqlite3.connect('players.db') as cursor:
            cursor.execute("INSERT INTO PLAYER (name, balance) VALUES ('" + name + "', " + str(balance) + ")")

    def delete_player(self, name):
        with sqlite3.connect('players.db') as cursor:
            cursor.execute("DELETE FROM PLAYER WHERE name = ?", (name,))

    def get_balance(self, name):
        with sqlite3.connect('players.db') as cursor:
            result = cursor.execute("SELECT balance FROM PLAYER WHERE name = ?", (name,)).fetchall()
        return result[0][0]

    def update_balance(self, name, new_balance):
        with sqlite3.connect('players.db') as cursor:
            cursor.execute("UPDATE PLAYER SET balance =  " + str(new_balance) + " WHERE name = ?", (name,))

    def get_players_list(self):
        with sqlite3.connect('players.db') as cursor:
            rows = cursor.execute("SELECT name FROM PLAYER ORDER BY name").fetchall()
        return [player[0] for player in rows]

    def check_if_player_exists(self, name):
        with sqlite3.connect('players.db') as cursor:
            result = cursor.execute("SELECT name FROM PLAYER WHERE name = ?", (name,)).fetchall()
        if result:
            return True
        else:
            return False

    def print_full_table(self):
        with sqlite3.connect('players.db') as cursor:
            rows = cursor.execute("SELECT * FROM PLAYER").fetchall()
        print(rows)

