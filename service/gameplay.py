def get_card_worth(value : str):
    worth_dict = {
        "Ace" : 11,
        "2" : 2,
        "3" : 3,
        "4" : 4,
        "5" : 5,
        "6" : 6,
        "7" : 7,
        "8" : 8,
        "9" : 9,
        "10" : 10,
        "Jack" : 10,
        "Queen" : 10,
        "King" : 10
    }
    return worth_dict[value]

def check_blackjack(hand):
    return len(hand.hand) == 2 and hand.get_total_worth() == 21

def check_bust(hand):
    return hand.get_total_worth() > 21

def display_hand(name, hand):
    print(name + "'s hand:")
    print(hand)