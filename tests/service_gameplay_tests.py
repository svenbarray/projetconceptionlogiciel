import unittest
from service.gameplay import get_card_worth
from service.gameplay import check_blackjack
from service.gameplay import check_bust

from business_object.hand import Hand
from business_object.card import Card

class ServiceGameplayTests(unittest.TestCase):

    def test_get_card_worth(self):
        self.assertEqual(get_card_worth("Ace"), 11)
        self.assertEqual(get_card_worth("4"), 4)
        self.assertEqual(get_card_worth("Jack"), 10)

    def test_check_blackjack(self):
        card1 = Card("Jack", "Clubs")
        card2 = Card("Ace", "Heart")
        card3 = Card("8", "Clubs")
        card4 = Card("3", "Diamonds")
        hand1 = Hand()
        hand2 = Hand()
        hand3 = Hand()
        hand1.hand = [card1, card2]
        hand2.hand = [card1, card3, card4]
        hand3.hand = [card1, card3]
        self.assertTrue(check_blackjack(hand1))
        self.assertFalse(check_blackjack(hand2))
        self.assertFalse(check_blackjack(hand3))

    def test_check_bust(self):
        card1 = Card("Jack", "Clubs")
        card2 = Card("Ace", "Heart")
        card3 = Card("8", "Clubs")
        card4 = Card("3", "Diamonds")
        hand1 = Hand()
        hand2 = Hand()
        hand3 = Hand()
        hand1.hand = [card1, card2, card3]
        hand2.hand = [card1, card3, card4]
        hand3.hand = [card1, card2, card3, card4]
        self.assertFalse(check_bust(hand1))
        self.assertFalse(check_bust(hand2))
        self.assertTrue(check_bust(hand3))

if __name__ == '__main__':
    unittest.main()