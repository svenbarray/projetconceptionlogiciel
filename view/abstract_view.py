from abc import ABC, abstractmethod
from view.session import Session

class AbstractView(ABC):
    @abstractmethod
    def make_choice(self):
        pass
