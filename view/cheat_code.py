from InquirerPy import inquirer
from InquirerPy.base.control import Choice
from view.abstract_view import AbstractView
from view.session import Session
from dao.player_dao import Player_DAO
from view.menu import Menu

class CheatCode(AbstractView):
    def make_choice(self):
        new_balance = input("Please choose your new balance. ")
        while not new_balance.isdigit():
            new_balance = input("The balance needs to be a positive integer. Please choose a valid starting balance. ")
        Player_DAO().update_balance(Session().user_name, int(new_balance))
        Session().balance = int(new_balance)
        print("Your balance is now " + new_balance + ".")
        return Menu()

