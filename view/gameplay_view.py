from InquirerPy import inquirer
from InquirerPy.base.control import Choice
from view.abstract_view import AbstractView
from view.session import Session
from dao.player_dao import Player_DAO
from view.menu import Menu
import time

from business_object.hand import Hand
from business_object.deck import Deck

from service.gameplay import display_hand
from service.gameplay import check_blackjack
from service.gameplay import check_bust

HIT_OR_STAND = inquirer.select(
            message = "HIT or STAND?",
            choices = [
                Choice("HIT"),
                Choice("STAND")
            ]
        )

ANOTHER_ROUND = inquirer.select(
            message = "Do you wish to go for another round?",
            choices = [
                Choice("YES"),
                Choice("NO")
            ]
        )

class Gameplay_View(AbstractView):

    Session().current_deck = Deck()
    #Limite: le deck de 312 cartes peut théoriquement se vider si le joueur enchaîne trop de parties.
    #On peut facilement vérifier si le deck a moins de 70 cartes restantes à chaque piocher et remélanger si c'est le cas, mais cela implique une perte significative de performance.

    def make_choice(self):
        bet = input("Your balance is " + str(Session().balance) + ". Please choose your bet. ")
        while not bet.isdigit() or int(bet) > Session().balance or int(bet) == 0:
            if not bet.isdigit():
                bet = input("The bet needs to be a positive integer. Your balance is " + str(Session().balance) + ". Please choose a valid bet. ")
            elif int(bet) > Session().balance:
                bet = input("The bet cannot be higher than your balance. Your balance is " + str(Session().balance) + ". Please choose a valid bet. ")
            else:
                bet = input("Your bet cannot be 0.  Your balance is " + str(Session().balance) + ". Please choose a valid bet. ")
        Session().bet = int(bet)
        self.start_game()

    def start_game(self):
        Session().player_hand = Hand()
        Session().player_hand.draw(Session().current_deck.deck_id)
        Session().player_hand.draw(Session().current_deck.deck_id)
        display_hand(Session().user_name, Session().player_hand)
        time.sleep(1)
        if check_blackjack(Session().player_hand):
            print("You got a Black Jack!")
            self.dealers_turn(player_score = 21, player_blackjack = True)
        else:
            self.hit_or_stand()

    def hit_or_stand(self):
        choice = HIT_OR_STAND.execute()
        if choice == "HIT":
            Session().player_hand.draw(Session().current_deck.deck_id)
            display_hand(Session().user_name, Session().player_hand)
            if check_bust(Session().player_hand):
                print("BUST! You lose!")
                self.scoring(player_score = Session().player_hand.get_total_worth(), dealer_score = 0)
            else:
                self.hit_or_stand()
        else:
            self.dealers_turn(player_score = Session().player_hand.get_total_worth())
    
    def dealers_turn(self, player_score, player_blackjack = False):
        dealer_blackjack = False
        Session().dealer_hand = Hand()
        Session().dealer_hand.draw(Session().current_deck.deck_id)
        Session().dealer_hand.draw(Session().current_deck.deck_id)
        display_hand("Dealer", Session().dealer_hand)
        time.sleep(1)
        if check_blackjack(Session().dealer_hand):
            print("The Dealer got a Black Jack!")
            dealer_blackjack = True
        else:
            hand_worth = Session().dealer_hand.get_total_worth()
            while hand_worth < 17:
                Session().dealer_hand.draw(Session().current_deck.deck_id)
                print("The dealer draws...")
                display_hand("Dealer", Session().dealer_hand)
                hand_worth = Session().dealer_hand.get_total_worth()
                time.sleep(1)
        self.scoring(player_score = player_score, dealer_score = Session().dealer_hand.get_total_worth(), player_blackjack = player_blackjack, dealer_blackjack = dealer_blackjack)

    def scoring(self, player_score, dealer_score, player_blackjack = False, dealer_blackjack = False):
        if player_blackjack and not dealer_blackjack:
            Session().balance += int(1.5*Session().bet)
            print("You got a Black Jack and the Dealer didn't. You win 1.5 times your bet and gain " + str(int(1.5*Session().bet)) + " credits! Your balance is now " + str(Session().balance) + ".")
        elif player_blackjack and dealer_blackjack:
            print("Both you and the Dealer got a Black Jack. Nothing happens.")
        elif player_score > 21:
            Session().balance -= Session().bet
            print("You got a BUST! You lose your bet and lose " + str(Session().bet) + " credits. Your balance is now " + str(Session().balance) + ".")
        elif dealer_score > 21:
            Session().balance += Session().bet
            print("The Dealer got a BUST! You win your bet and gain " + str(Session().bet) + " credits. Your balance is now " + str(Session().balance) + ".")
        elif player_score == dealer_score:
            print("Both you and the Dealer got the same score. Nothing happens.")
        elif player_score < dealer_score:
            Session().balance -= Session().bet
            print("You got a lower score than the dealer. You lose your bet and lose " + str(Session().bet) + " credits. Your balance is now " + str(Session().balance) + ".")
        else:
            Session().balance += Session().bet
            print("You got a higher score than the dealer You win your bet and gain " + str(Session().bet) + " credits. Your balance is now " + str(Session().balance) + ".")
        self.another_round()

    def another_round(self):
        Session().player_hand = None
        Session().dealer_hand = None
        choice = ANOTHER_ROUND.execute()
        if choice == "YES":
            self.make_choice()
        else:
            print("Thank you play playing!")
            from view.menu import Menu
            return Menu()
            #For some reason, this command does not bring you back to the menu but just ends the script completely. Fortunatly, this bug is not harmful to the user experience



