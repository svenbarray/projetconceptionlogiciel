from InquirerPy import inquirer
from InquirerPy.base.control import Choice
from view.abstract_view import AbstractView
from view.session import Session
from dao.player_dao import Player_DAO

class LogIn(AbstractView):
    def initialize_selection(self):
        player_list = Player_DAO().get_players_list()
        choices = [Choice(player) for player in player_list]
        choices.append("BACK TO STARTING SCREEN")
        player_selection = inquirer.select(
                    message = "Which player would you like to embody?",
                    choices = choices)
        return player_selection

    def make_choice(self):
        player_selection = self.initialize_selection()
        selected_player = player_selection.execute()
        if selected_player == "BACK TO STARTING SCREEN":
            from view.start_view import StartView
            return StartView()
        else:
            Session().user_name = selected_player
            Session().balance = Player_DAO().get_balance(selected_player)
            from view.menu import Menu
            return Menu()


