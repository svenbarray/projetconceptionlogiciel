from InquirerPy import inquirer
from InquirerPy.base.control import Choice
from view.abstract_view import AbstractView
from view.session import Session
from dao.player_dao import Player_DAO

class Menu(AbstractView):
    def initialize_selection(self):
        menu_selection = inquirer.select(
            message = "Welcome, " + Session().user_name  + ". Your current balance is " + str(Session().balance) + ". What would you like to do?",
            choices = [
                Choice("Start a Black Jack game"),
                Choice("Cheat code (change balance at will)"),
                Choice("Log out")
            ]
        )
        return menu_selection

    def make_choice(self):
        menu_selection = self.initialize_selection()
        selection = menu_selection.execute()
        if selection == "Start a Black Jack game":
            if Session().balance == 0:
                print("You can not start a game with a balance of 0. Use Cheat codes to change your balance.")
                selection = menu_selection.execute()
            else:
                from view.gameplay_view import Gameplay_View
                return Gameplay_View()
        elif selection == "Cheat code (change balance at will)":
            from view.cheat_code import CheatCode
            return CheatCode()
        else:
            from view.start_view import StartView
            return StartView()


