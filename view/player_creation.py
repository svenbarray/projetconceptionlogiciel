from InquirerPy import inquirer
from InquirerPy.base.control import Choice
from view.abstract_view import AbstractView
from view.session import Session
from dao.player_dao import Player_DAO
from view.start_view import StartView

class PlayerCreation(AbstractView):
    def make_choice(self):
        selected_name = input("Choose a name for the new player. Type 'CANCEL' to cancel. ")
        if selected_name.lower() == "cancel":
            return StartView()

        if selected_name == "BACK TO STARTING SCREEN":
            #Limites vis-à-vis des potentiels noms de joueur également utilisés comme commandes de menu
            print("Nice try, dude.")
            return StartView()

        invalid_name = Player_DAO().check_if_player_exists(selected_name)
        while invalid_name:
            selected_name = input("This player already exists. Please choose another name. Type 'CANCEL' to cancel. ")
            if selected_name.lower() == "cancel":
                return StartView()
            invalid_name = Player_DAO().check_if_player_exists(selected_name)

        starting_balance = input("Please choose " + selected_name + "'s starting balance. ")
        while not starting_balance.isdigit():
            starting_balance = input("The balance needs to be a positive integer. Please choose a valid starting balance. ")

        Player_DAO().insert_player(selected_name, starting_balance)
        print("Player '" + selected_name + "' successfully created with a starting balance of " + starting_balance + ".")

        return StartView()

