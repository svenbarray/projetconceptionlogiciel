from InquirerPy import inquirer
from InquirerPy.base.control import Choice
from view.abstract_view import AbstractView
from view.session import Session
from dao.player_dao import Player_DAO

class PlayerDeletion(AbstractView):
    def initialize_selection(self):
        player_list = Player_DAO().get_players_list()
        choices = [Choice(player) for player in player_list]
        choices.append("BACK TO STARTING SCREEN")
        player_selection = inquirer.select(
                    message = "Which player would you like to delete?",
                    choices = choices)
        return player_selection

    def make_choice(self):
        player_selection = self.initialize_selection()
        selected_player = player_selection.execute()
        if selected_player != "BACK TO STARTING SCREEN":
            Player_DAO().delete_player(selected_player)
            print("Player " + selected_player + " successfully deleted.")
        from view.start_view import StartView
        return StartView()


