from utils.singleton import Singleton
from business_object.hand import Hand
from business_object.deck import Deck

class Session(metaclass=Singleton):
    def __init__(self):
        self.user_name: str = None
        self.balance: int = None
        self.current_deck: Deck = None
        self.bet: int = None
        self.player_hand: Hand = None
        self.dealer_hand: Hand = None