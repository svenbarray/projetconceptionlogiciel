from InquirerPy import inquirer
from InquirerPy.base.control import Choice
from view.abstract_view import AbstractView
from view.session import Session

class StartView(AbstractView):
    def __init__(self):
        self.__questions = inquirer.select(
            message="Welcome to BlackJack3000. What do you want to do?",
            choices=[
                Choice("Embody a player"),
                Choice("Create a player"),
                Choice("Delete a player"),
                Choice("EXIT")]
        )

    def make_choice(self):
        response = self.__questions.execute()
        if response == "Embody a player":
            from view.log_in import LogIn
            return LogIn()
        elif response == "Create a player":
            from view.player_creation import PlayerCreation
            return PlayerCreation()
        elif response == "Delete a player":
            from view.player_deletion import PlayerDeletion
            return PlayerDeletion()
        else:
            print("Thank you for using BlackJack3000!")

